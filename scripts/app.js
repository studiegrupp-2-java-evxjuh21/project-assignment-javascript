let menuOpen = false;
const hamburgerBtn = document.getElementById('hamburger'); /* Hamburger-button*/
const mobileMenu = document.querySelector(".mobile-menu"); /* Mobile-menu */
const links = document.getElementsByClassName('mobile-link'); /* The nav links */

/* Function to toggle the menu on and off */
function menuToggle() {
    if (!menuOpen) {
        hamburgerBtn.classList.add("open");
        menuOpen = true;
        mobileMenu.style.bottom = "0%";
        closeContactForm();
    } else {
        hamburgerBtn.classList.remove("open");
        menuOpen = false;
        mobileMenu.style.bottom = "-100%";
    }
}

/* EventListener for the Hamburger-button */
window.addEventListener("load", function() {
    hamburgerBtn.addEventListener("click", menuToggle);

    /* When a navlink is clicked, close the menu */
    for (let i = 0; i < links.length; i++) {
        links[i].addEventListener("click", menuToggle)
    }
});

/*  

    START OF HIDE MENU ON SCROLL

*/

// Variable to keep track of our previous position on the page
let lastScroll = 0;

window.addEventListener("scroll", () => {

    // If mobile menu is not open
    if (!menuOpen) {

        // Create a HTML-object of header
        const header = document.getElementsByClassName("header");

        // If our current scroll value on Y-axis is greater then our previos scroll value AND our current scroll is at least 115px
        // that means we're scrolling down and want to hide the header.
        // Else, we're going up and want to show our header 
        if (window.scrollY > lastScroll && window.scrollY >= 115) {
            header[0].style.top = "-115px";
        } else {
            header[0].style.top = "0";
        }

        // lastScroll is assigned the value of our current scroll value on the Y-axis in order to be able to evaluate next time we scroll
        lastScroll = window.scrollY;
    }
});

/*  

    END OF HIDE MENU ON SCROLL

*/

/*  

    START OF LIGHTBOX

*/

let currentImg;

// Create modal
const modalOverlay = document.createElement("div");
modalOverlay.classList.add("modal-overlay");

// Close button for our modal overlay
const closeModal = document.createElement("a");
closeModal.setAttribute("id", "close-modal");
closeModal.append("\u00d7");

// Create lightbox
const lightboxGalleryContainer = document.createElement("div");
lightboxGalleryContainer.classList.add("lightbox-container");

// Previous button for lightbox
const prevButton = document.createElement("a");
prevButton.setAttribute("id", "previous-button");
prevButton.setAttribute("onclick", "prevNext(-1)");
prevButton.append("❮");

// Next button for lightbox
const nextButton = document.createElement("a");
nextButton.setAttribute("id", "next-button");
nextButton.setAttribute("onclick", "prevNext(1)");
nextButton.append("❯");

// Lightbox image
const lightboxImg = document.createElement("img");
lightboxImg.setAttribute("width", "500px");
lightboxImg.classList.add("lightbox-image");

// Thumbnail container for lightbox
const lightboxThumbnailContainer = document.createElement("div");
lightboxThumbnailContainer.classList.add("lightbox-thumbnail-container");

// Section for gallery
const gallerySection = document.createElement("section");
gallerySection.setAttribute("id", "gallery-section");
document.body.insertBefore(gallerySection, document.getElementById("contact-us"));

// Header for gallery
const galleryHeader = document.createElement("div");
galleryHeader.classList.add("gallery-header");
gallerySection.append(galleryHeader);

// h3 for Gallery
const galleryh3 = document.createElement("h3");
galleryh3.append("NÅGRA NÖJDA HYRESGÄSTER");
galleryHeader.append(galleryh3);

// Gallery
const gallery = document.createElement("div");
gallery.classList.add("gallery");
gallerySection.append(gallery);



// Array for our images
const imgObjArray = [{
        src: "./media/img/gallery/grey-cat.jpg",
        alt: "Cat",
        width: "100px",
        class: "gallery-thumbnail",
    },
    {
        src: "./media/img/gallery/vackra-close.jpg",
        alt: "Close picture of dog",
        width: "100px",
        class: "gallery-thumbnail",
    },
    {
        src: "./media/img/gallery/grey-comfy.jpg",
        alt: "Cat in bed",
        width: "100px",
        class: "gallery-thumbnail",
    },
    {
        src: "./media/img/gallery/astor-box.jpg",
        alt: "Cat in box",
        width: "100px",
        class: "gallery-thumbnail",
    },
    {
        src: "./media/img/gallery/dante.jpg",
        alt: "Dog in nature",
        width: "100px",
        class: "gallery-thumbnail",
    },
    {
        src: "./media/img/gallery/melvin.jpg",
        alt: "Cat",
        width: "100px",
        class: "gallery-thumbnail",
    },
    {
        src: "./media/img/gallery/vackra-by-lake.jpg",
        alt: "Dog by lake",
        width: "100px",
        class: "gallery-thumbnail",
    },
    {
        src: "./media/img/gallery/vackra-in-nature.jpg",
        alt: "Dog in nature",
        width: "100px",
        class: "gallery-thumbnail",
    },
    {
        src: "./media/img/gallery/cuteaf.PNG",
        alt: "Cat sticking out tongue",
        width: "100px",
        class: "gallery-thumbnail",
    },
    {
        src: "./media/img/gallery/kissekatte.PNG",
        alt: "Playful cat",
        width: "100px",
        class: "gallery-thumbnail",
    }
];

// Loop through array in order to create img and append to gallery and to lightbox-thumbnail-container
imgObjArray.forEach((image, index) => {
    const thumbnailGallery = document.createElement("img");
    thumbnailGallery.setAttribute("src", image.src);
    thumbnailGallery.setAttribute("alt", image.alt);
    thumbnailGallery.setAttribute("width", image.width);
    thumbnailGallery.setAttribute("class", image.class);
    thumbnailGallery.setAttribute("onclick", `openLightbox(${index})`);
    gallery.append(thumbnailGallery);

    const thumbnailLightbox = document.createElement("img");
    thumbnailLightbox.setAttribute("src", image.src);
    thumbnailLightbox.setAttribute("alt", image.alt);
    thumbnailLightbox.setAttribute("width", "70px");
    thumbnailLightbox.setAttribute("class", "lightbox-thumbnail");
    thumbnailLightbox.setAttribute("onclick", `changeImg(${index})`);
    lightboxThumbnailContainer.append(thumbnailLightbox);
});

// Fetching our newly created img-elements as HTMLCollection so we can reference them in functions
const galleryThumbnails = document.getElementsByClassName("gallery-thumbnail");
const lightboxThumbnails = document.getElementsByClassName("lightbox-thumbnail");

// This function exists only to minimize repetition and organizing code
const appendLightbox = () => {

    // Appending modal-overlay to be the first child of body
    document.body.insertBefore(modalOverlay, document.body.firstChild);

    // Appending lightbox to modal-overlay
    modalOverlay.append(closeModal, prevButton, lightboxGalleryContainer, nextButton);

    // Appending lightbox-image and lightbox-thumbnail-container to lightbox-gallery-container
    lightboxGalleryContainer.append(lightboxImg, lightboxThumbnailContainer);
}

// This function exists only to minimize repetition and organizing code
const removeLightbox = () => {

    // Removing lightbox-gallery-container from DOM
    lightboxGalleryContainer.remove();

    // Removing previous-button and next-button form DOM
    prevButton.remove();
    nextButton.remove();
}

// This function exists only to minimize repetition and organizing code
const removeThumbnailClass = () => {

    // Looping through our HTMLCollection lightboxThumbnails and removes the active-thumbnail class
    for (const img of lightboxThumbnails) {
        img.classList.remove("active-thumbnail");
    }
}

// This function exists only to minimize repetition and organizing code
const addThumbnailClass = (index) => {

    // Adds the active-thumbnail class to the lightbox-thumbnail
    lightboxThumbnails[index].classList.add("active-thumbnail");
}

// Function to hide or show previous-button and next-button depending on the index of the current image
const hidePrevNextButton = () => {

    // To make the code more readable the last index of lightboxThumbnails is assigned to this variable
    let numberOfIndexes = lightboxThumbnails.length - 1;

    switch (currentImg) {

        // If the currentImg has index 0, execute the following code and hide previous-button
        // next-button is set to visible in order to avoid a bug that can occur if the "edge" images are opened and then closed
        case 0:
            prevButton.style.visibility = "hidden";
            prevButton.style.transition = "0s";
            nextButton.style.visibility = "visible";
            nextButton.style.transition = "0.5s";
            break;

            // If the currentImg has the last index, execute the following code and hide next-button
            // prev-button is set to visible in order to avoid a bug that can occur if the "edge" images are opened and then closed
        case numberOfIndexes:
            nextButton.style.visibility = "hidden";
            nextButton.style.transition = "0s";
            prevButton.style.visibility = "visible";
            prevButton.style.transition = "0.5s";
            break;

            // For all other indexes, show both buttons
        default:
            prevButton.style.visibility = "visible";
            prevButton.style.transition = "0.5s";
            nextButton.style.visibility = "visible";
            nextButton.style.transition = "0.5s";
    }
}

// Event listener for close-modal
closeModal.addEventListener("click", () => {

    // This functions call makes sure that we don't have any thumbnails with the class active-thumbnail attached to it
    removeThumbnailClass();

    // Removes the lightbox from DOM
    removeLightbox();

    // Setting text of modalP to be an empty string
    modalP.textContent = "";
    h2.textContent = "";

    // Removing closeModal from DOM
    closeModal.remove();

    // Removing openModal from DOM
    openModal.remove();

    // Removes the modal-overlay from DOM
    modalOverlay.remove();

});

// onclick function from gallery images 
const openLightbox = (index) => {

    // currentImg is set to whatever index was passed from gallery image
    currentImg = index;

    // Lightbox is added to the DOM
    appendLightbox();

    // Hide or show previous-button and next-button depending on the value of currentImg
    hidePrevNextButton();

    // Lightbox-image src attribute is set to whatever the src of the image clicked was
    lightboxImg.setAttribute("src", galleryThumbnails[index].src);

    // Adds the active-thumbnail class to the corresponding img-element in lightbox-thumbnail
    addThumbnailClass(index);

}

// onclick function from lightbox-thumbnail
const changeImg = (index) => {

    // currentImg is set to whatever index was passed from gallery image
    currentImg = index;

    // Hide or show previous-button and next-button depending on the value of currentImg
    hidePrevNextButton();

    // Removes the active-thumbnail class from all lightbox-thumbnails
    removeThumbnailClass();

    // Adds the active-thumbnail class to the corresponding img-element in lightbox-thumbnail
    addThumbnailClass(index);

    // Lightbox-image src attribute is set to whatever the src of the image clicked was
    lightboxImg.setAttribute("src", lightboxThumbnails[index].src);

}

// onclick function for previous-button and next-button. Direction can be either -1 or 1
const prevNext = (direction) => {

    // The value of direction is added to the current value of currentImg. If the index of the last clicked img is 7 and direction is -1, currentImg new value is 6 because:
    // 7+(-1) = 6
    currentImg += direction;

    // Hide or show previous-button and next-button depending on the value of currentImg
    hidePrevNextButton();

    // If next-button is pressed when we're at the end of the HTMLCollection lightboxThumbnails, currentImg is set to length of lightboxThumbnails -1 (the user is unable to scroll froward any further)
    if (currentImg === (lightboxThumbnails.length - 1) && direction === 1) currentImg = lightboxThumbnails.length - 1;

    // If previous-button is pressed when we're at the beginning of the HTMLCollection lightboxThumbnail, currentImg is set to 0 (the user is unable to scroll backwards any further).
    if (currentImg === -1 && direction === -1) currentImg = 0;

    // Removes the active-thumbnail class from all lightbox-thumbnails
    removeThumbnailClass();

    // Adds the active-thumbnail class to the corresponding img-element in lightbox-thumbnail
    addThumbnailClass(currentImg);

    // Lightbox-image src attribute is set to whatever the src of the image clicked was
    lightboxImg.setAttribute("src", lightboxThumbnails[currentImg].src);

}

/*  

    END OF LIGHTBOX

*/

/*  

   START OF FORM

*/

// We create the div that will act as a container for our "form".
const formContainer = document.createElement("form");
formContainer.setAttribute("id", "form-container");
formContainer.setAttribute("onsubmit", "sendForm(); return false;")

// We create the div that will act as the header for our "form".
const formHeader = document.createElement("div");
formHeader.setAttribute("id", "form-header");

// We create the paragraph that will act as the "text" in our "form".
// We then append the text to the paragraph. And then we append/insert the paragraph into the "formHeader".
const formHeaderText = document.createElement("p");
formHeaderText.append("Chatta med oss");
formHeader.append(formHeaderText);

// We create the "a"-tag that will act as out close/minimize-button in the form. 
// We give this button an onclick that will call upon the closeContactForm()-function.
// We then append/insert the button into the "formHeader".
const closeForm = document.createElement("a");
closeForm.setAttribute("id", "close-form");
closeForm.setAttribute("onclick", "closeContactForm()");
closeForm.append("\u2212");
formHeader.append(closeForm);

// We create the div that will act as the container for the main-content of our "form".
const formContent = document.createElement("div");
formContent.setAttribute("id", "form-content");

// We create a paragraph that will display some text for the user.
const formContentText = document.createElement("p");
formContentText.append("Hej! Vad kul att du har hittat hit! Här kan du ställa frågor om våra tjänster eller få reda på mer om oss som företag. Vi svarar så fort vi kan.");

// We create a label ("Mail").
const formMailLabel = document.createElement("label");
formMailLabel.setAttribute("id", "mail-label");
formMailLabel.append("Mail");

// We create an input-field for the "Mail". 
const formMailInput = document.createElement("input");
formMailInput.setAttribute("id", "email-input");
formMailInput.setAttribute("type", "text");
formMailInput.setAttribute("name", "email");
formMailInput.setAttribute("placeholder", "Din email...");

// We create a label ("Ämne").
const formSubjectLabel = document.createElement("label");
formSubjectLabel.setAttribute("id", "subject-label");
formSubjectLabel.append("Ämne");

// We create an input-field for the "Ämne"
const formSubjectInput = document.createElement("input");
formSubjectInput.setAttribute("id", "subject-input");
formSubjectInput.setAttribute("type", "text");
formSubjectInput.setAttribute("name", "subject");
formSubjectInput.setAttribute("placeholder", "Ämne...");

// We create a label ("Meddelande").
const formMessageLabel = document.createElement("label");
formMessageLabel.setAttribute("id", "message-label");
formMessageLabel.append("Meddelande");

// We create a textarea for the message.
const formMessageArea = document.createElement("textarea");
formMessageArea.setAttribute("id", "textarea-input");
formMessageArea.setAttribute("name", "message");
formMessageArea.setAttribute("placeholder", "Ditt meddelande");

// We create a button for our form. We give this button a text of "Skicka" and also an "onclick" which calls the sendForm()-function.
const submitButton = document.createElement("button");
submitButton.setAttribute("id", "submit-button");
submitButton.setAttribute("type", "submit");
submitButton.append("Skicka");

// Now we are done with all the parts of the form. It's time to append.

// And then we put the content of the form inside of the form.
formContent.append(formContentText, formMailLabel, formMailInput, formSubjectLabel, formSubjectInput, formMessageLabel, formMessageArea, submitButton);

// We put the formHeader and formContent inside of the formContainer. (All the contents of the formHeader are appended directly when it's created above).
formContainer.append(formHeader, formContent);

// We add the button that will open the form. Fixed button to the right side of the screen.
// We give the button an "onlick" that calls the openContactForm()-function. We also import a fontawesome icon by creating an i-element.
const openFormButton = document.createElement("button");
openFormButton.setAttribute("id", "open-form-button");
openFormButton.setAttribute("onclick", "openContactForm()");
const openFormIcon = document.createElement("i");
openFormIcon.classList.add("fa");
openFormIcon.classList.add("fa-comments");
openFormButton.append(openFormIcon);

// We append/add the "openFormButton" to our page.
document.body.append(openFormButton);

// We create the div/container that will hold the text, loader & close-button when the button ("Skicka"-button) inside of the form is clicked.
const onSubmitContainer = document.createElement("div");
onSubmitContainer.setAttribute("id", "on-submit-container");

// We create the close-button for the "onSubmitContainer". We give in an "onclick" that will call on the closeModal1()-function.
const onSubmitCloseButton = document.createElement("span");
onSubmitCloseButton.classList.add("close");
onSubmitCloseButton.setAttribute("onclick", "closeModal1()");
onSubmitCloseButton.append("\u00d7");

// We create our "loader" for the "onSubmitContainer".
const onSubmitLoader = document.createElement("div");
onSubmitLoader.classList.add("loader");

// We create our "text" for the "onSubmitContainer". We set it's default text to ("Vänligen vänta medan vi behandlar din information...").
const onSubmitText = document.createElement("p");
onSubmitText.setAttribute("id", "on-submit-text");
onSubmitText.textContent = "Vänligen vänta medan vi behandlar din information...";

// And then we append/add the close-button, loader and text inside of the "onSubmitContainer".
onSubmitContainer.append(onSubmitCloseButton, onSubmitLoader, onSubmitText);

const openContactForm = () => {
    formContainer.style.animationName = "moveOut";
    document.body.append(formContainer);
    openFormButton.remove();
}

const closeContactForm = () => {
    formContainer.style.animationName = "moveIn";

    setTimeout(function() {
        document.body.append(openFormButton);
        formContainer.remove();
    }, 480);
}

const sendForm = () => {
    document.body.insertBefore(modalOverlay, document.body.firstChild);
    modalOverlay.append(onSubmitContainer);
    onSubmitCloseButton.style.display = "none";
    onSubmitLoader.style.display = "block";

    const eMail = formMailInput.value;
    const searchedEmail = eMail.search(/\S+@\S+\./);

    setTimeout(function() {
        onSubmitLoader.style.display = "none";
    }, 3000);

    setTimeout(function() {
        if (searchedEmail < 0) {
            onSubmitText.textContent = "Vänligen ange en korrekt mailadress!";
            onSubmitCloseButton.style.display = "block";
        } else {
            onSubmitText.textContent = "Tack. Vi har tagit emot ditt meddelande!";
            onSubmitCloseButton.style.display = "block";
            formMailInput.value = "";
            formSubjectInput.value = "";
            formMessageArea.value = "";
        }
    }, 3000);

}

// We create a function that gets executed each time we click the "onSubmitCloseButton" inside of the modal. 
// This button, when clicked, will close the modal.
const closeModal1 = () => {
    onSubmitContainer.remove();
    modalOverlay.remove();
    onSubmitText.textContent = "Vänligen vänta medan vi behandlar din information...";
}

/*  

   END OF FORM

*/

/*  

   START OF EMPLOYEES

*/
// Create openModal for opening a modal employee card.
const openModal = document.createElement("div");
openModal.classList.add("open-modal");

// Create h2 for header so head text can be added into modal employee card.
const h2 = document.createElement("header");
h2.classList.add("head");

// Create modalP for paragraph so text can be added into modal employee card.
const modalP = document.createElement("p");
modalP.classList.add("modal-para");

// Create modal image for employees.
const aboutImg = document.createElement("img");
aboutImg.setAttribute("width", "300px");
aboutImg.setAttribute("height", "300px")
aboutImg.classList.add("modal-image");
/* const aboutEmployees = document.createElement("div");
aboutEmployees.classList.add("employees-list"); */


// Array-object for employees.
const employees = [{
        name: "Sara Nord",
        filename: "aboutone.jpg",
        alt: "img",
        content: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dolor mollitia nobis, optio corrupti dolorem nostrum distinctio vel, fugiat iste perferendis aspernatur ut," +
            "sunt illo necessitatibus inventore quo natus. Natus suscipit laboriosam Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dolor mollitia nobis, optio corrupti dolorem nostrum distinctio vel, fugiat iste perferendis aspernatur ut," +
            "sunt illo necessitatibus inventore quo natus. Natus suscipit laboriosam",
        occupation: "VD",
        width: "300px",
    },
    {
        name: "David Sundin",
        filename: "abouttwo.jpg",
        alt: "img",
        content: "Praesentium quidem sit deserunt enimullam, veritatis odit labore omnis molestias consectetur atque totam autem repellendus esse? Sed" +
            "eum beatae deserunt cum obcaecati earum, nulla ducimus adipisci ex magnam porro itaque" +
            "asperiores hic consequatur veritatis recusandae ab delectus? Consequatur iste odit, illum vero" +
            "optio tempora repudiandae perferendis autem amet! Natus perspiciatis ipsam neque, alias officia" +
            "quae, reprehenderit ex nemo eius sunt aliquam maiores totam consequuntur voluptates qui?",
        width: "300px",
        occupation: "Platsansvarig",

    },
    {
        name: "Anna Dahl",
        filename: "aboutthree.jpg",
        alt: "img",
        content: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dolor mollitia nobis, optio corrupti" +
            "d!olorem nostrum distinctio vel, fugiat iste perferendis aspernatur ut, sunt illo necessitatibus!" +
            "inventore quo natus. Natus suscipit laboriosam a provident magnam repellat eligendi cupiditate" +
            "soluta, incidunt quasi reprehenderit, quibusdam omnis. Praesentium quidem sit deserunt enim" +
            "ullam, veritatis odit labore omnis molestias consectetur atque totam autem repellendus esse? Sed" +
            "eum beatae deserunt cum obcaecati earum, nulla ducimus adipisci ex magnam porro itaque" +
            "asperiores hic consequatur veritatis recusandae ab delectus? Consequatur iste odit, illum vero" +
            "optio tempora repudiandae perferendis autem amet! Natus perspiciatis ipsam neque, alias officia" +
            "quae, reprehenderit ex nemo eius sunt aliquam maiores totam consequuntur voluptates qui? Ea," +
            "aspernatur minus praesentium possimus est velit excepturi, id harum ipsa architecto beatae.",
        width: "300px",
        occupation: "Koordinator",
    },
    {
        name: "Charleen Mason",
        filename: "aboutfour.jpg",
        alt: "img",
        content: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dolor mollitia nobis, optio corrupti" +
            "d!olorem nostrum distinctio vel, fugiat iste perferendis aspernatur ut, sunt illo necessitatibus!" +
            "inventore quo natus. Natus suscipit laboriosam a provident magnam repellat eligendi cupiditate" +
            "soluta, incidunt quasi reprehenderit, quibusdam omnis. Praesentium quidem sit deserunt enim" +
            "ullam, veritatis odit labore omnis molestias consectetur atque totam autem repellendus esse? Sed" +
            "eum beatae deserunt cum obcaecati earum, nulla ducimus adipisci ex magnam porro itaque" +
            "asperiores hic consequatur veritatis recusandae ab delectus? Consequatur iste odit, illum vero" +
            "optio tempora repudiandae perferendis autem amet! Natus perspiciatis ipsam neque, alias officia" +
            "quae, reprehenderit ex nemo eius sunt aliquam maiores totam consequuntur voluptates qui? Ea," +
            "aspernatur minus praesentium possimus est velit excepturi, id harum ipsa architecto beatae.",
        width: "300px",
        occupation: "Förvaltare",
    }
];


const employeesElements = employees.map((img, index) => `<div class="show-employees"onclick="appendModal(${index})"><img alt="${img.alt}" src="./media/img/${img.filename}" ><p>${img.name}<br><span>${img.occupation}</span></p></div>`).join("");
window.addEventListener("load", () => {
    document.querySelector(".about-text-wrapper").innerHTML = `<div id="about-h2">Lär känna oss! <br></div>` + employeesElements;
});

// Create a function with switch index case that shows each modal employee card when click on show-employees.
const appendModal = (index) => {

    // Appending modal-overlay to be the first child of body
    document.body.insertBefore(modalOverlay, document.body.firstChild);

    // Appending employee card to modal-overlay
    modalOverlay.append(openModal, closeModal);
    openModal.append(h2, aboutImg, modalP);

    switch (index) {
        case 0:

            aboutImg.setAttribute("src", `./media/img/${employees[0].filename}`)
            modalP.append(employees[0].content);
            h2.append(`Om mig - ${employees[0].name}`);
            break;

        case 1:

            aboutImg.setAttribute("src", `./media/img/${employees[1].filename}`)
            modalP.append(employees[1].content);
            h2.append(`Om mig - ${employees[1].name}`);
            break;

        case 2:
            aboutImg.setAttribute("src", `./media/img/${employees[2].filename}`)
            modalP.append(employees[2].content);
            h2.append(`Om mig - ${employees[2].name}`);
            break;

        case 3:
            aboutImg.setAttribute("src", `./media/img/${employees[3].filename}`)
            modalP.append(employees[3].content);
            h2.append(`Om mig - ${employees[3].name}`);
            break;
    }
}

/*

   END OF EMPLOYEES

*/